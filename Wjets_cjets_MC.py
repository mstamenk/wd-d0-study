#import section
import os, ROOT

#ATLAS style for plotting
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasLabels.C")
ROOT.SetAtlasStyle()

pwd = os.getenv('MYROOT') + '/'
path_to_samples = pwd + 'samples/'

#W_jets c_jets file
filename = 'MG_Wjets_cjets_emu.root'

f = ROOT.TFile(path_to_samples + filename, 'read')

#OS histogram for D meson mass
hist_name = 'el_minus_SR_0tag_Dplus_OS_Dmeson_m'
h_el_minus_1tag_OS = f.Get(hist_name)

#SS histogram for D meson mass
hist_name = 'el_minus_SR_0tag_Dplus_SS_Dmeson_m'
h_el_minus_1tag_SS = f.Get(hist_name)

#Create histogram for OS-SS
h_el_minus_1tag_OS_SS = h_el_minus_1tag_OS.Clone(h_el_minus_1tag_OS.GetName() + '_OS_SS')
h_el_minus_1tag_OS_SS.Add(h_el_minus_1tag_SS,-1) # Substract the SS histogram
#OS-SS
h_el_minus_1tag_OS_SS.SetLineColor(ROOT.kBlue)
h_el_minus_1tag_OS_SS.SetMarkerSize(0)

# Make plot look nice together on the canvas
factor = 1.8
h_el_minus_1tag_OS_SS.SetMaximum(factor * h_el_minus_1tag_OS_SS.GetMaximum())

legend1 = ROOT.TLegend(0.6,0.5,0.9,0.7) # Place legend in the top right corner, starting just after the middle
legend1.SetBorderSize(0)

legend1.AddEntry(h_el_minus_1tag_OS_SS, 'Wjets_cjets','f')

c1 = ROOT.TCanvas()
h_el_minus_1tag_OS_SS.Draw('hist e OS-SS')
legend1.Draw()

ROOT.ATLASLabel(0.2,0.88,"Internal") # ATLAS label for internal plots
ROOT.myText(0.2,0.82,1,"#sqrt{s}= 13 TeV, %s fb^{-1}"%('139'))
ROOT.myText(0.2,0.76,1,'D-meson, D^{+}#rightarrow K#pi#pi, e^{-}-channel, 0 b-tags')

# Save plot
c1.Print('C_example_OS_SS_Wcjets.pdf')