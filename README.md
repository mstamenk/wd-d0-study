# Framework to study the D0->K pi pi pi decay using the outputs of the W+D analysis
The goal of this framework is to gather pieces of code to study / plot the outputs of the W+D analysis. 

Author: Marko Stamenkovic [marko.stamenkovic@cern.ch](mailto:marko.stamenkovic@cern.ch), Miha Muskijna [miha.muskinja@cern.ch](mailto:miha.muskinja@cern.ch), Riley Clark [rileyclark@berkeley.edu](rileyclark@berkeley.edu)

# Instructions to get started

Please see the following [documentation](https://docs.nersc.gov/connect/) to connect on a cori machine using an `ssh` connection.

Once connected to a cori machine, setup the atlas environment, based on the following [documentation](https://atlaswiki.lbl.gov/software/atlascori). From a fresh terminal:
```
source /global/project/projectdirs/atlas/scripts/setupATLAS.sh
setupATLAS -c centos7+batch
cd wd-d0-study
source setup.sh
```

# Cloning the framework

Install the framework from gitlab using 
```
git clone ssh://git@gitlab.cern.ch:7999/mstamenk/wd-d0-study.git
cd wd-d0-study
source setup.sh
```

# Run simple plotting script 
Run simple plotting maccro
```
python plot_var.py # to run directly and produce plots

python -i plot_var.py # to run interactively, if XQuartz is installed, will show the plot on the computer
```
