# Script to plot shapes of Powheg, Sherpa and MG
import ROOT,os

ROOT.gROOT.SetBatch(ROOT.kTRUE) # set batch mode to produce the plots


# Get ATLAS style for plots
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasLabels.C") 

ROOT.SetAtlasStyle()

# regions
jets = {'2jet': ['2jet'],
        '3pjet' : ['3jet','4pjet'],
        '3jet' : ['3jet']
        }
# labels for plots
labels = {'2jet' : '2 jets',
          '3jet' : '3 jets',
          '3pjet' : '3+ jets',
          '1tag' : '1 c-tag',
          '2tag' : '2 c-tag',
          '150_250ptv' : '150 < pTV < 250 GeV',
          '250ptv' : 'pTV > 250 GeV',
        }

# output path directory for plots
eos_plots = '/data/atlas/users/mstamenk/Plots/shapes-Sh-MG-Py/'
if not os.path.exists(eos_plots):
    os.makedirs(eos_plots)

# get histogram from file, if merging required adds histograms (for example 3+jets = 3 jets + 4+ jets
def get_hist(hist,f,process,tag,jet,ptv):
    for p in process:
        for j in jets[jet]:
            h_name = '%s_%s%s_%s_SR_OneMuMbbTT'%(p,tag,j,ptv)
            try:
                h_tmp = f.Get(h_name)
                hist.Add(h_tmp)
            except:
                print "No %s"%h_name

# set hist style
def set_hist(h,color):
    h.SetMarkerSize(0)
    h.SetLineWidth(2)
    h.SetLineColor(color)
    h.Rebin(2)
    h.GetXaxis().SetRangeUser(50,210)
    h.GetXaxis().SetLabelOffset(999)
    h.GetXaxis().SetLabelSize(0)

def set_max(*hists):
    maxi = 0
    for h in hists:
        maxi = max(maxi,h.GetMaximum())
    for h in hists:
        h.SetMaximum(1.8*maxi)
    return maxi

# set ratio style for nice ratio plot on canvas
def set_ratio_hist(*hists):
    maxi = 0
    for h_div in hists:
        h_div.GetXaxis().SetTitle('m(cc) [GeV]')
        h_div.GetXaxis().SetTitleSize(0.11)
        h_div.GetXaxis().SetTitleOffset(1.35)
        h_div.GetXaxis().SetLabelSize(0.11)
        h_div.GetXaxis().SetLabelOffset(0.03)
        h_div.GetYaxis().SetTitleSize(0.11)
        h_div.GetYaxis().SetTitleOffset(0.35)
        h_div.GetYaxis().SetLabelSize(0.11)
        h_div.GetYaxis().SetLabelOffset(0.001)
        h_div.GetYaxis().SetMaxDigits(0)
        h_div.GetYaxis().SetNdivisions(4,8,0,ROOT.kTRUE)
        maxi = max(maxi,h_div.GetMaximum())
    for h_div in hists:
        h_div.GetYaxis().SetRangeUser(0.,2.)

    
# get 3 files to compare same variable
MG_path = '/data/atlas/users/mstamenk/CxAOD/r32-15/VHcc-2L-100720-MGPy8/Zjets_MGPy8.root'
PwPy8_path = '/data/atlas/users/mstamenk/CxAOD/r32-15/VHcc-2L-Zjets-Py8/Zjets_Py8.root'
Sh_path = '/data/atlas/users/mstamenk/Fit/vhcc_2lep/r32-15/LimitHistograms.VH.llcc.13TeV.mc16ade.NIKHEF.v22-sys-dt-4.root'

f_MG = ROOT.TFile(MG_path)
f_PwPy8 = ROOT.TFile(PwPy8_path)
f_Sh = ROOT.TFile(Sh_path)

# Process to be compared (histogram name in file)
process_Sh = ['Zcc','Zcl','Zl','Zbb','Zbc','Zbl']
process_MG = ['Mad%s'%s for s in process_Sh]
process_PwPy8 = ['Zee','Zmumu']

# This is for testing not used in the end
tag = '1tag'
jet = '2jet'
ptv = '150_250ptv'

# get all plots for the regions and do the histogram comparison + ratios
for tag in ['1tag','2tag']:
    for jet in ['2jet','3pjet']:
        for ptv in ['150_250ptv','250ptv']:

            name_Sh = 'Sherpa_%s%s_%s'%(tag,jet,ptv)
            h_Sh = ROOT.TH1F(name_Sh,name_Sh,100,0,500)
            get_hist(h_Sh,f_Sh,process_Sh,tag,jet,ptv)

            name_MG = 'MGerpa_%s%s_%s'%(tag,jet,ptv)
            h_MG = ROOT.TH1F(name_MG,name_MG,100,0,500)
            get_hist(h_MG,f_MG,process_MG,tag,jet,ptv)

            name_PwPy8 = 'PwPy8erpa_%s%s_%s'%(tag,jet,ptv)
            h_PwPy8 = ROOT.TH1F(name_PwPy8,name_PwPy8,100,0,500)
            get_hist(h_PwPy8,f_PwPy8,process_PwPy8,tag,jet,ptv)

            set_hist(h_Sh,ROOT.kRed + 1)
            set_hist(h_MG,ROOT.kBlue + 1)
            set_hist(h_PwPy8,ROOT.kGreen + 2)

            set_max(h_Sh,h_MG,h_PwPy8)

            h_div_MG = h_MG.Clone(h_MG.GetName()+'_ratio')
            h_div_MG.Divide(h_Sh)
            h_div_PwPy8 = h_PwPy8.Clone(h_PwPy8.GetName()+'_ratio')
            h_div_PwPy8.Divide(h_Sh)

            set_ratio_hist(h_div_MG,h_div_PwPy8)


            c = ROOT.TCanvas()
            legend = ROOT.TLegend(0.6,0.6,0.9,0.9)
            legend.SetBorderSize(0)
            p1 = ROOT.TPad("c_1","",0,0,1,0.3)
            p2 = ROOT.TPad("c_2","", 0,0.3,1,1)
            p1.Draw()
            p2.Draw()
            p1.SetBottomMargin(0.3)
            p1.SetTopMargin(0.05)
            p1.SetRightMargin(0.05)
            p2.SetTopMargin(0.05)
            p2.SetBottomMargin(0.02)
            p2.SetRightMargin(0.05)

            legend.AddEntry(h_Sh,'Sherpa')
            legend.AddEntry(h_MG,'MGPy8')
            legend.AddEntry(h_PwPy8,'PwPy8')

            p2.cd()
            h_Sh.Draw('hist e')
            h_MG.Draw('hist e same')
            h_PwPy8.Draw('hist e same')
            legend.Draw()
            ROOT.ATLASLabel(0.2,0.86,"Internal",1)
            ROOT.myText(0.2,0.78,1,'2-lepton, Z+jets')
            ROOT.myText(0.2,0.70,1,'%s, %s, %s'%(labels[tag],labels[jet],labels[ptv]))

            p1.cd()
            p1.SetGridy()
            h_div_MG.Draw('hist e')
            h_div_PwPy8.Draw('hist e same')

            c.Print(eos_plots + '/' + name_Sh + '.pdf')
