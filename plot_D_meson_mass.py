#import section
import os, ROOT

#ATLAS style for plotting
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasLabels.C")
ROOT.SetAtlasStyle()

pwd = os.getenv('MYROOT') + '/'
path_to_samples = pwd + 'samples/'

#files
filename1 = 'data.root'
filename2 = 'MG_Wjets_bjets_emu.root'
filename3 = 'MG_Wjets_cjets_emu.root'
filename4 = 'MG_Zjets_bjets_emu.root'
filename5 = 'MG_Zjets_cjets_emu.root'
filename6 = 'MG_Zjets_light_emu.root'
filename7 = 'MG_Wjets_emu.root'
filename8 = 'MG_Wjets_tau.root'
filename9 = 'MG_Wjets_light_emu.root'
filename10 = 'Top_single_top.root'
filename11 = 'Top_ttbar.root'
filename12 = 'MG_Zjets_tau.root'
filename13 = 'Diboson.root'

f1 = ROOT.TFile(path_to_samples + filename1, 'read')
f2 = ROOT.TFile(path_to_samples + filename2, 'read')
f3 = ROOT.TFile(path_to_samples + filename3, 'read')
f4 = ROOT.TFile(path_to_samples + filename4, 'read')
f5 = ROOT.TFile(path_to_samples + filename5, 'read')
f6 = ROOT.TFile(path_to_samples + filename6, 'read')
f7 = ROOT.TFile(path_to_samples + filename7, 'read')
f8 = ROOT.TFile(path_to_samples + filename8, 'read')
f9 = ROOT.TFile(path_to_samples + filename9, 'read')
f10 = ROOT.TFile(path_to_samples + filename10, 'read')
f11 = ROOT.TFile(path_to_samples + filename11, 'read')
f12 = ROOT.TFile(path_to_samples + filename12, 'read')
f13 = ROOT.TFile(path_to_samples + filename13, 'read')

#OS histogram for D meson mass
hist_name = 'el_minus_SR_0tag_Dplus_OS__Dmeson_m'
h_el_minus_1tag_OS1 = f1.Get(hist_name)
h_el_minus_1tag_OS2 = f2.Get(hist_name)
h_el_minus_1tag_OS3 = f3.Get(hist_name)
h_el_minus_1tag_OS4 = f4.Get(hist_name)
h_el_minus_1tag_OS5 = f5.Get(hist_name)
h_el_minus_1tag_OS6 = f6.Get(hist_name)
h_el_minus_1tag_OS7 = f7.Get(hist_name)
h_el_minus_1tag_OS8 = f8.Get(hist_name)
h_el_minus_1tag_OS9 = f9.Get(hist_name)
h_el_minus_1tag_OS10 = f10.Get(hist_name)
h_el_minus_1tag_OS11 = f11.Get(hist_name)
h_el_minus_1tag_OS12 = f12.Get(hist_name)
h_el_minus_1tag_OS13 = f13.Get(hist_name)

#SS histogram for D meson mass
hist_name = 'el_minus_SR_0tag_Dplus_SS__Dmeson_m'
h_el_minus_1tag_SS1 = f1.Get(hist_name)
h_el_minus_1tag_SS2 = f2.Get(hist_name)
h_el_minus_1tag_SS3 = f3.Get(hist_name)
h_el_minus_1tag_SS4 = f4.Get(hist_name)
h_el_minus_1tag_SS5 = f5.Get(hist_name)
h_el_minus_1tag_SS6 = f6.Get(hist_name)
h_el_minus_1tag_SS7 = f7.Get(hist_name)
h_el_minus_1tag_SS8 = f8.Get(hist_name)
h_el_minus_1tag_SS9 = f9.Get(hist_name)
h_el_minus_1tag_SS10 = f10.Get(hist_name)
h_el_minus_1tag_SS11 = f11.Get(hist_name)
h_el_minus_1tag_SS12 = f12.Get(hist_name)
h_el_minus_1tag_SS13 = f13.Get(hist_name)


#Create histogram for OS-SS
h_el_minus_1tag_OS_SS1 = h_el_minus_1tag_OS1.Clone(h_el_minus_1tag_OS1.GetName() + '_OS_SS1')
h_el_minus_1tag_OS_SS1.Add(h_el_minus_1tag_SS1,-1) # Substract the SS histogram
h_el_minus_1tag_OS_SS2 = h_el_minus_1tag_OS2.Clone(h_el_minus_1tag_OS2.GetName() + '_OS_SS2')
h_el_minus_1tag_OS_SS2.Add(h_el_minus_1tag_SS2,-1)
h_el_minus_1tag_OS_SS3 = h_el_minus_1tag_OS3.Clone(h_el_minus_1tag_OS3.GetName() + '_OS_SS3')
h_el_minus_1tag_OS_SS3.Add(h_el_minus_1tag_SS3,-1) # Substract the SS histogram
h_el_minus_1tag_OS_SS4 = h_el_minus_1tag_OS4.Clone(h_el_minus_1tag_OS4.GetName() + '_OS_SS4')
h_el_minus_1tag_OS_SS4.Add(h_el_minus_1tag_SS4,-1)
h_el_minus_1tag_OS_SS5 = h_el_minus_1tag_OS5.Clone(h_el_minus_1tag_OS5.GetName() + '_OS_SS5')
h_el_minus_1tag_OS_SS5.Add(h_el_minus_1tag_SS5,-1)
h_el_minus_1tag_OS_SS6 = h_el_minus_1tag_OS6.Clone(h_el_minus_1tag_OS6.GetName() + '_OS_SS1')
h_el_minus_1tag_OS_SS6.Add(h_el_minus_1tag_SS6,-1) # Substract the SS histogram
h_el_minus_1tag_OS_SS7 = h_el_minus_1tag_OS7.Clone(h_el_minus_1tag_OS7.GetName() + '_OS_SS2')
h_el_minus_1tag_OS_SS7.Add(h_el_minus_1tag_SS7,-1)
h_el_minus_1tag_OS_SS8 = h_el_minus_1tag_OS8.Clone(h_el_minus_1tag_OS8.GetName() + '_OS_SS3')
h_el_minus_1tag_OS_SS8.Add(h_el_minus_1tag_SS8,-1) # Substract the SS histogram
h_el_minus_1tag_OS_SS9 = h_el_minus_1tag_OS9.Clone(h_el_minus_1tag_OS9.GetName() + '_OS_SS4')
h_el_minus_1tag_OS_SS9.Add(h_el_minus_1tag_SS9,-1)
h_el_minus_1tag_OS_SS10 = h_el_minus_1tag_OS10.Clone(h_el_minus_1tag_OS10.GetName() + '_OS_SS5')
h_el_minus_1tag_OS_SS10.Add(h_el_minus_1tag_SS10,-1)
h_el_minus_1tag_OS_SS11 = h_el_minus_1tag_OS11.Clone(h_el_minus_1tag_OS11.GetName() + '_OS_SS4')
h_el_minus_1tag_OS_SS11.Add(h_el_minus_1tag_SS11,-1)
h_el_minus_1tag_OS_SS12 = h_el_minus_1tag_OS12.Clone(h_el_minus_1tag_OS12.GetName() + '_OS_SS5')
h_el_minus_1tag_OS_SS12.Add(h_el_minus_1tag_SS12,-1)
h_el_minus_1tag_OS_SS13 = h_el_minus_1tag_OS13.Clone(h_el_minus_1tag_OS13.GetName() + '_OS_SS5')
h_el_minus_1tag_OS_SS13.Add(h_el_minus_1tag_SS13,-1)

h_el_minus_1tag_OS_SS1.Rebin(4)
h_el_minus_1tag_OS_SS2.Rebin(4)
h_el_minus_1tag_OS_SS3.Rebin(4)
h_el_minus_1tag_OS_SS4.Rebin(4)
h_el_minus_1tag_OS_SS5.Rebin(4)
h_el_minus_1tag_OS_SS6.Rebin(4)
h_el_minus_1tag_OS_SS7.Rebin(4)
h_el_minus_1tag_OS_SS8.Rebin(4)
h_el_minus_1tag_OS_SS9.Rebin(4)
h_el_minus_1tag_OS_SS10.Rebin(4)
h_el_minus_1tag_OS_SS11.Rebin(4)
h_el_minus_1tag_OS_SS12.Rebin(4)
h_el_minus_1tag_OS_SS13.Rebin(4)

hs = ROOT.THStack("hs", "DMeson Mass Histogram")

#OS-SS
    #data 1
#h_el_minus_1tag_OS_SS1.SetFillColor(ROOT.kBlue + 2)
h_el_minus_1tag_OS_SS1.SetMarkerStyle(20)
h_el_minus_1tag_OS_SS1.SetMarkerSize(0.7)
hs.Add(h_el_minus_1tag_OS_SS1)
    #W+c 3
h_el_minus_1tag_OS_SS3.SetFillColor(ROOT.kAzure - 4)
hs.Add(h_el_minus_1tag_OS_SS3)
    #W+b
h_el_minus_1tag_OS_SS2.SetFillColor(ROOT.kAzure - 9)
hs.Add(h_el_minus_1tag_OS_SS2)
    #W
h_el_minus_1tag_OS_SS7.SetFillColor(ROOT.kAzure - 5)
hs.Add(h_el_minus_1tag_OS_SS7)
    #Other 8,9
h_el_minus_1tag_OS_SS8.SetFillColor(ROOT.kMagenta - 10)
hs.Add(h_el_minus_1tag_OS_SS8)
h_el_minus_1tag_OS_SS9.SetFillColor(ROOT.kMagenta - 10)
hs.Add(h_el_minus_1tag_OS_SS9)
    #Z 4,5,6,12
h_el_minus_1tag_OS_SS4.SetFillColor(ROOT.kMagenta - 10)
hs.Add(h_el_minus_1tag_OS_SS4)
h_el_minus_1tag_OS_SS5.SetFillColor(ROOT.kMagenta - 10)
hs.Add(h_el_minus_1tag_OS_SS5)
h_el_minus_1tag_OS_SS12.SetFillColor(ROOT.kMagenta - 10)
hs.Add(h_el_minus_1tag_OS_SS12)
    #Top 10,11
h_el_minus_1tag_OS_SS10.SetFillColor(ROOT.kOrange - 5)
hs.Add(h_el_minus_1tag_OS_SS10)
h_el_minus_1tag_OS_SS11.SetFillColor(ROOT.kOrange - 5)
hs.Add(h_el_minus_1tag_OS_SS11)
    #Diboson
h_el_minus_1tag_OS_SS13.SetFillColor(ROOT.kRed - 3)
hs.Add(h_el_minus_1tag_OS_SS13)

# Make plot look nice together on the canvas
factor = 1.8
h_el_minus_1tag_OS_SS1.SetMaximum(factor * h_el_minus_1tag_OS_SS1.GetMaximum())
h_el_minus_1tag_OS_SS2.SetMaximum(factor * h_el_minus_1tag_OS_SS2.GetMaximum())
h_el_minus_1tag_OS_SS3.SetMaximum(factor * h_el_minus_1tag_OS_SS3.GetMaximum())
h_el_minus_1tag_OS_SS4.SetMaximum(factor * h_el_minus_1tag_OS_SS4.GetMaximum())
h_el_minus_1tag_OS_SS5.SetMaximum(factor * h_el_minus_1tag_OS_SS5.GetMaximum())
h_el_minus_1tag_OS_SS6.SetMaximum(factor * h_el_minus_1tag_OS_SS6.GetMaximum())
h_el_minus_1tag_OS_SS7.SetMaximum(factor * h_el_minus_1tag_OS_SS7.GetMaximum())
h_el_minus_1tag_OS_SS8.SetMaximum(factor * h_el_minus_1tag_OS_SS8.GetMaximum())
h_el_minus_1tag_OS_SS9.SetMaximum(factor * h_el_minus_1tag_OS_SS9.GetMaximum())
h_el_minus_1tag_OS_SS10.SetMaximum(factor * h_el_minus_1tag_OS_SS10.GetMaximum())
h_el_minus_1tag_OS_SS11.SetMaximum(factor * h_el_minus_1tag_OS_SS11.GetMaximum())
h_el_minus_1tag_OS_SS12.SetMaximum(factor * h_el_minus_1tag_OS_SS12.GetMaximum())
h_el_minus_1tag_OS_SS13.SetMaximum(factor * h_el_minus_1tag_OS_SS13.GetMaximum())
legend1 = ROOT.TLegend(0.7,0.55,0.85,0.85) # Place legend in the top right corner, starting just after the middle
legend1.SetTextSize(0.05)
legend1.SetTextFont(132)
legend1.SetBorderSize(0)

h_el_minus_1tag_OS_SS1.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS2.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS3.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS4.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS5.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS6.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS7.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS8.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS9.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS10.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS11.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS12.GetXaxis().SetRangeUser(1.7,2.2)
h_el_minus_1tag_OS_SS13.GetXaxis().SetRangeUser(1.7,2.2)

h_el_minus_1tag_OS_SS2.SetLineWidth(0)
h_el_minus_1tag_OS_SS3.SetLineWidth(0)
h_el_minus_1tag_OS_SS4.SetLineWidth(0)
h_el_minus_1tag_OS_SS5.SetLineWidth(0)
h_el_minus_1tag_OS_SS6.SetLineWidth(0)
h_el_minus_1tag_OS_SS7.SetLineWidth(0)
h_el_minus_1tag_OS_SS8.SetLineWidth(0)
h_el_minus_1tag_OS_SS9.SetLineWidth(0)
h_el_minus_1tag_OS_SS10.SetLineWidth(0)
h_el_minus_1tag_OS_SS11.SetLineWidth(0)
h_el_minus_1tag_OS_SS12.SetLineWidth(0)
h_el_minus_1tag_OS_SS13.SetLineWidth(0)

h_el_minus_1tag_OS_SS1.GetXaxis().SetTitle('D^{\pm} mass [GeV]')
h_el_minus_1tag_OS_SS1.GetYaxis().SetTitle('Entries / GeV')
h_el_minus_1tag_OS_SS3.GetXaxis().SetLabelOffset(999)
h_el_minus_1tag_OS_SS3.GetXaxis().SetLabelSize(0)

legend1.AddEntry(h_el_minus_1tag_OS_SS1, 'Data','p')
legend1.AddEntry(h_el_minus_1tag_OS_SS7, 'W','f')
legend1.AddEntry(h_el_minus_1tag_OS_SS3, 'W+c','f')
legend1.AddEntry(h_el_minus_1tag_OS_SS2, 'W+b','f')
legend1.AddEntry(h_el_minus_1tag_OS_SS4, 'Other','f')
legend1.AddEntry(h_el_minus_1tag_OS_SS10, 'Top','f')
legend1.AddEntry(h_el_minus_1tag_OS_SS13, 'Diboson','f')

h_el_minus_1tag_OS_SS2.SetLabelOffset(999)
h_el_minus_1tag_OS_SS3.SetLabelOffset(999)
h_el_minus_1tag_OS_SS4.SetLabelOffset(999)
h_el_minus_1tag_OS_SS5.SetLabelOffset(999)
h_el_minus_1tag_OS_SS6.SetLabelOffset(999)
h_el_minus_1tag_OS_SS7.SetLabelOffset(999)
h_el_minus_1tag_OS_SS8.SetLabelOffset(999)
h_el_minus_1tag_OS_SS9.SetLabelOffset(999)
h_el_minus_1tag_OS_SS10.SetLabelOffset(999)
h_el_minus_1tag_OS_SS11.SetLabelOffset(999)
h_el_minus_1tag_OS_SS12.SetLabelOffset(999)
h_el_minus_1tag_OS_SS13.SetLabelOffset(999)

#Ratio plot

#sum the histograms
h_sum1 = h_el_minus_1tag_OS_SS2.Clone(h_el_minus_1tag_OS_SS2.GetName() + '+2')
h_sum1.Add(h_el_minus_1tag_OS_SS3, 1)
h_sum2 = h_sum1.Clone(h_sum1.GetName() + '+3')
h_sum2.Add(h_el_minus_1tag_OS_SS4, 1)
h_sum3 = h_sum2.Clone(h_sum2.GetName() + '+4')
h_sum3.Add(h_el_minus_1tag_OS_SS5, 1)
h_sum4 = h_sum3.Clone(h_sum3.GetName() + '+5')
h_sum4.Add(h_el_minus_1tag_OS_SS6, 1)
h_sum5 = h_sum4.Clone(h_sum4.GetName() + '+6')
h_sum5.Add(h_el_minus_1tag_OS_SS7, 1)
h_sum6 = h_sum5.Clone(h_sum5.GetName() + '+7')
h_sum6.Add(h_el_minus_1tag_OS_SS8, 1)
h_sum7 = h_sum1.Clone(h_sum1.GetName() + '+8')
h_sum7.Add(h_el_minus_1tag_OS_SS9, 1)
h_sum8 = h_sum7.Clone(h_sum7.GetName() + '+9')
h_sum8.Add(h_el_minus_1tag_OS_SS10, 1)
h_sum9 = h_sum8.Clone(h_sum8.GetName() + '+10')
h_sum9.Add(h_el_minus_1tag_OS_SS11, 1)
h_sum10 = h_sum9.Clone(h_sum9.GetName() + '+11')
h_sum10.Add(h_el_minus_1tag_OS_SS12, 1)
h_sum_mc = h_sum10.Clone(h_sum10.GetName() + '+12')
h_sum_mc.Add(h_el_minus_1tag_OS_SS13, 1)

#ratio histogram
h_ratio = h_el_minus_1tag_OS_SS1.Clone(h_el_minus_1tag_OS_SS1.GetName() + 'ratio')
h_ratio.Divide(h_sum_mc)

#set ratio bin to 1
h_mc_error_ratio = h_ratio.Clone(h_ratio.GetName()+ '_mc_error')
for i in range(h_ratio.GetNbinsX()):
    h_mc_error_ratio.SetBinContent(i, 1) # set the ratio bin to 1 in all bins
    h_mc_error_ratio.SetBinError(i, h_sum_mc.GetBinError(i))

h_ratio.GetYaxis().SetRangeUser(0.5,1.5)
h_ratio.SetFillStyle(0)
h_mc_error_ratio.SetFillStyle(0)
h_ratio.GetYaxis().SetNdivisions(4,8,0,ROOT.kTRUE)
h_ratio.GetXaxis().SetTitle('m(D^{\pm}) [GeV]')
h_ratio.GetXaxis().SetTitleSize(0.11)
h_ratio.GetXaxis().SetTitleOffset(1.35)
h_ratio.GetXaxis().SetLabelSize(0.11)
h_ratio.GetXaxis().SetLabelOffset(0.03)
h_ratio.GetYaxis().SetTitle('Data / MC')
h_ratio.GetYaxis().SetTitleSize(0.1)
h_ratio.GetYaxis().SetTitleOffset(0.37)
h_ratio.GetYaxis().SetLabelSize(0.11)
h_ratio.GetYaxis().SetLabelOffset(0.001)
h_ratio.GetYaxis().SetMaxDigits(0)
h_ratio.GetYaxis().SetNdivisions(4,8,0,ROOT.kTRUE)
h_ratio.SetMarkerSize(0.7)


c = ROOT.TCanvas()
p1 = ROOT.TPad("c_1","",0,0,1,0.315)
p2 = ROOT.TPad("c_2","", 0,0.315,1,1.)
p1.Draw()
p2.Draw()
p1.SetBottomMargin(0.3)
p1.SetTopMargin(0.05)
p1.SetRightMargin(0.05)
p2.SetTopMargin(0.06)
p2.SetBottomMargin(0.02)
p2.SetRightMargin(0.05)

p2.cd()
h_el_minus_1tag_OS_SS7.Draw('hist')
h_el_minus_1tag_OS_SS3.Draw('hist same')
h_el_minus_1tag_OS_SS11.Draw('hist same')
h_el_minus_1tag_OS_SS10.Draw('hist same')
h_el_minus_1tag_OS_SS2.Draw('hist same')
h_el_minus_1tag_OS_SS13.Draw('hist same')
h_el_minus_1tag_OS_SS9.Draw('hist same')
h_el_minus_1tag_OS_SS4.Draw('hist same')
h_el_minus_1tag_OS_SS6.Draw('hist same')
h_el_minus_1tag_OS_SS8.Draw('hist same')
h_el_minus_1tag_OS_SS5.Draw('hist same')
h_el_minus_1tag_OS_SS12.Draw('hist same')
h_el_minus_1tag_OS_SS1.Draw('lep same')
legend1.Draw()

ROOT.ATLASLabel(0.2,0.88,"Internal") # ATLAS label for internal plots
ROOT.myText(0.2,0.82,1,"#sqrt{s}= 13 TeV, %s fb^{-1}"%('139'))
ROOT.myText(0.2,0.76,1,'D-meson, D^{+}#rightarrow K#pi#pi, e^{-}-channel, 0 b-tags')

p1.cd()
p1.SetGridy()
h_ratio.Draw('e')







# Save plot
c.Print('Dmeson_m_dataMC_ratio.pdf')





