# Script to plot one variable from the outputs of the W+D analysis
# author: Marko Stamenkovic

# import section
import os, ROOT

# Setup ATLAS style for plotting
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasLabels.C")
ROOT.SetAtlasStyle()


def createCanvasPads():
    c = ROOT.TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0)
    pad1.SetFillStyle(4000)
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0.00, 1, 0.3)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.1 / 0.3)
    pad2.SetFillStyle(4000)
    pad2.Draw()

    return c, pad1, pad2


def set_axis_size(h):
    r = 0.3 / 0.7

    # x axis
    h.GetXaxis().SetLabelSize(h.GetXaxis().GetLabelSize() / r)
    h.GetXaxis().SetTitleSize(h.GetXaxis().GetTitleSize() / r)
    h.GetXaxis().SetTitleOffset(h.GetXaxis().GetTitleOffset() * r + 0.5)

    # y axis
    h.GetYaxis().SetLabelSize(h.GetYaxis().GetLabelSize() / r)
    h.GetYaxis().SetTitleSize(h.GetYaxis().GetTitleSize() / r)
    h.GetYaxis().SetTitleOffset(h.GetYaxis().GetTitleOffset() * r)
    h.GetYaxis().SetNdivisions(506)
    h.GetYaxis().SetTitle("Ratio")


# Initialise the file from which to read the histograms
pwd = os.getenv('MYROOT') + '/' # get the path to the repository where the 'source setup.sh' was done
path_to_samples = pwd + 'samples/' # path to samples folder

filename = 'data.root' # example here using the data

f = ROOT.TFile(path_to_samples + filename, 'read') # read the file with the histograms

# Getting one histogram to be plotted

# Get the histogram of the opposite sign, negative electron, 0 tag category
hist_name = 'el_minus_SR_0tag_Dplus_OS__Dmeson_m' # D-meson mass
h_el_minus_0tag_OS = f.Get(hist_name)

# Get the histogram of the opposite sign, negative muon, 0 tag category
hist_name = 'mu_minus_SR_0tag_Dplus_OS__Dmeson_m' # D-meson mass
h_mu_minus_0tag_OS = f.Get(hist_name)

# Normalize histograms
h_el_minus_0tag_OS.Scale(1. / h_el_minus_0tag_OS.GetSumOfWeights())
h_mu_minus_0tag_OS.Scale(1. / h_mu_minus_0tag_OS.GetSumOfWeights())

# Rebin
h_el_minus_0tag_OS.Rebin(4)
h_mu_minus_0tag_OS.Rebin(4)

# Prepare the plots
h_el_minus_0tag_OS.SetLineColor(ROOT.kBlue + 2) # Change line color for plot
h_mu_minus_0tag_OS.SetLineColor(ROOT.kRed + 2)
h_el_minus_0tag_OS.SetMarkerSize(0)
h_mu_minus_0tag_OS.SetMarkerSize(0)

# Make plot look nice together on the canvas
factor = 1.5
maxi = max(h_el_minus_0tag_OS.GetMaximum(), (h_mu_minus_0tag_OS.GetMaximum()))
h_el_minus_0tag_OS.SetMaximum(factor * maxi)
h_mu_minus_0tag_OS.SetMaximum(factor * maxi)

# Create legend for first plot
legend1 = ROOT.TLegend(0.7, 0.925 - 2 * 0.06, 0.95, 0.925) # Place legend in the top right corner, starting just after the middle
legend1.SetBorderSize(0)
legend1.SetFillColor(0)
legend1.SetFillStyle(0)
legend1.SetTextSize(28)
legend1.SetTextFont(43)

legend1.AddEntry(h_el_minus_0tag_OS, 'e^{-} OS','l')
legend1.AddEntry(h_mu_minus_0tag_OS, '#mu^{-} OS', 'l')

# Plot histograms: first canvas, draw OS and SS on the same plot
c1, pad1, pad2 = createCanvasPads()
pad1.cd()
h_el_minus_0tag_OS.Draw('hist e') # Draw histogram, with error bar on the bins
h_mu_minus_0tag_OS.Draw('hist e same') # Draw second hist on the same canvas

# set x axis range
h_el_minus_0tag_OS.GetXaxis().SetRangeUser(1.71, 2.2)
h_mu_minus_0tag_OS.GetXaxis().SetRangeUser(1.71, 2.2)

# set title
h_el_minus_0tag_OS.GetYaxis().SetTitle("Entries")

# draw legend
legend1.Draw()

# Add text in the plot
ROOT.ATLASLabel(0.2,0.88,"Internal") # ATLAS label for internal plots
ROOT.myText(0.2,0.82,1,"#sqrt{s}= 13 TeV, %s fb^{-1}"%('139'))
ROOT.myText(0.2,0.76,1,'D-meson, D^{+}#rightarrow K#pi#pi, 0 b-tags')

# bottom pannel
pad2.cd()

# make a new histogram for the ratio
h_el_minus_0tag_ratio = h_el_minus_0tag_OS.Clone("%s_ratio" % h_el_minus_0tag_OS.GetName())

# divide
h_el_minus_0tag_ratio.Divide(h_mu_minus_0tag_OS)

# set title
h_el_minus_0tag_ratio.GetXaxis().SetTitle("m(D+) [GeV]")

# set y-axis range in the ratio
h_el_minus_0tag_ratio.GetYaxis().SetRangeUser(0.91, 1.09)

# set label / axis font size
set_axis_size(h_el_minus_0tag_ratio)

# make horizontal line at y = 1
left_bin = h_el_minus_0tag_ratio.FindBin(1.71)
right_bin = h_el_minus_0tag_ratio.FindBin(2.2)
x_left = h_el_minus_0tag_ratio.GetBinCenter(left_bin) - 0.5 * h_el_minus_0tag_ratio.GetBinWidth(left_bin)
x_right = h_el_minus_0tag_ratio.GetBinCenter(right_bin) + 0.5 * h_el_minus_0tag_ratio.GetBinWidth(right_bin)
line = ROOT.TLine(x_left, 1, x_right, 1.0)

# draw the ratio
h_el_minus_0tag_ratio.Draw("hist e")
line.Draw()

# Save plot
c1.Print('C_example_OS_SS_ratio_plot.pdf')




