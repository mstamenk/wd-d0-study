import ROOT, os
from array import array

ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasLabels.C")
ROOT.SetAtlasStyle()

path_to_files = '/global/homes/r/rclark/gitlabrepo/wd-d0-study/'
filename = 'output_run_tree.root'

f = ROOT.TFile(path_to_files + filename)


#D0 MASS
hist_name = 'm'
h_D0mass = f.Get(hist_name)
factor = 1.8 
h_D0mass.SetMaximum(factor * h_D0mass.GetMaximum())
h_D0mass.GetXaxis().SetRangeUser(1800, 2000)
#Mass legend
legend1 = ROOT.TLegend(0.7,0.55,0.85,0.85)
legend1.SetTextSize(0.05)
legend1.SetTextFont(132)
legend1.SetBorderSize(0)
legend1.AddEntry(h_D0mass, 'D0', 'l')
#Axes
h_D0mass.GetXaxis().SetTitle('D^{0} mass [GeV]')
h_D0mass.GetYaxis().SetTitle('Entries / GeV')
h_D0mass.SetFillStyle(0)
#Mass Canvas
c1 = ROOT.TCanvas()
h_D0mass.Draw('hist')
legend1.Draw()
ROOT.ATLASLabel(0.2,0.88,"Internal") 
ROOT.myText(0.2,0.82,1,"#sqrt{s}= 13 TeV, %s fb^{-1}"%('139'))
#ROOT.myText(0.2,0.76,1,'D-meson, D^{0}#rightarrow K#pi#pi, e^{-}-channel, 0 b-tags')

#D0 PT
hist_name = 'pt'
h_D0pT = f.Get(hist_name)
#PT legend
legend2 = ROOT.TLegend(0.7,0.55,0.85,0.85)
legend2.SetTextSize(0.05)
legend2.SetTextFont(132)
legend2.SetBorderSize(0)
legend2.AddEntry(h_D0pT, 'pT', 'l')
#Axes
h_D0pT.GetXaxis().SetTitle('D^{0} pT [MeV]')
h_D0pT.GetYaxis().SetTitle('Entries / MeV')
#
c2 = ROOT.TCanvas()
h_D0pT.Draw('hist')
ROOT.ATLASLabel(0.2,0.88,"Internal") 
legend2.Draw()
#Need to write script for filtering out events which have
#pdgID == 321(K+) 
#pdgID == 211 (pi+) or pdgID == -211 (pi-)