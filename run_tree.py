<<<<<<< HEAD
import ROOT, os
from array import array

ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasLabels.C")
ROOT.SetAtlasStyle()
=======
# Script to run on a tree of the CharmAnalysis: Single Particle Gun (SPG) for D0

# Goal for Riley: plot D-meson mass, plot D-meson pT for 10 000 events containing a true D0 going to K pi pi pi 

import ROOT, os
from array import array

>>>>>>> f5b0fdd22a948105a207a3f308c0d9ecb799d346

path_to_files = '/global/cfs/cdirs/atlas/wcharm/v10_spg_D0/merged/'
filename = 'SPG.ForcedDecay.999911.root'

tree = ROOT.TChain('CharmAnalysis')
tree.AddFile(path_to_files + filename)

outfile = ROOT.TFile('output_run_tree.root','recreate')

h_m = ROOT.TH1F('m','m',50, 1700,2200)
<<<<<<< HEAD
h_p = ROOT.TH1F('pt','pt',50, 5000,100000)
h_D0_decaymright = ROOT.TH1F('m_D0_right', 'm_D0_right', 50, 1700, 2200)
h_D0_decaymwrong = ROOT.TH1F('m_D0_wrong', 'm_D0_wrong', 50, 1700, 2200)
mass_k = 493.677 #MeV
mass_pi = 139.570 #MeV



=======
>>>>>>> f5b0fdd22a948105a207a3f308c0d9ecb799d346

stop_iteration = 0
for event in tree:
    if event.DMesons_NOSYS_m.size() > 0 :
<<<<<<< HEAD
        pdgID = [i for i in event.DMesons_NOSYS_pdgId]

        if pdgID.count(421) == 1:
            indexD0 = pdgID.index(421)
            mass = [m for m in event.DMesons_NOSYS_m]
            pt = [p for p in event.DMesons_NOSYS_pt]
            daughter_pdgID = [d for d in event.DMesons_NOSYS_daughterInfo__pdgId.at(indexD0)]
           
        
           
            
            if len(daughter_pdgID) == 4: 
                if daughter_pdgID.count(-321) == 1 and daughter_pdgID.count(211) == 2 and daughter_pdgID.count(-211) == 1:
                    h_m.Fill(mass[0])
                    
                    print('D0 mass', mass[0])

                    track_pt = [i for i in event.DMesons_NOSYS_daughterInfo__pt.at(indexD0)]
                    track_eta = [i for i in event.DMesons_NOSYS_daughterInfo__eta.at(indexD0)]
                    track_phi = [i for i  in event.DMesons_NOSYS_daughterInfo__phi.at(indexD0)]

                    vec_D0_right = ROOT.TLorentzVector()
                    vec_K_minus = ROOT.TLorentzVector()
                    vec_pi_plus1 = ROOT.TLorentzVector()
                    vec_pi_plus2 = ROOT.TLorentzVector()
                    vec_pi_minus = ROOT.TLorentzVector()
                    vec_D0_wrong = ROOT.TLorentzVector()
                    vec_K_minus_wrong = ROOT.TLorentzVector()
                    vec_pi_plus1_wrong = ROOT.TLorentzVector()
                    vec_pi_plus2_wrong = ROOT.TLorentzVector()
                    vec_pi_minus_wrong = ROOT.TLorentzVector()

                    print('pdgid', pdgID)
                    print('daughter pdgid', daughter_pdgID)
                    print('track pt:', track_pt)
                    print('track eta:', track_eta)
                    print('track phi:', track_phi)
                    
                    index_k= daughter_pdgID.index(-321)
                    index_piminus = daughter_pdgID.index(-211)
                    index_piplus1 = daughter_pdgID.index(211)
                    indices = [0, 1, 2, 3]
                    indices.remove(index_k)
                    indices.remove(index_piminus)
                    indices.remove(index_piplus1)
                    index_piplus2 = indices[0]
                    #print(index_k ,index_piminus, index_piplus1, index_piplus2)

                    vec_K_minus.SetPtEtaPhiM(track_pt[index_k], track_eta[index_k],track_phi[index_k], mass_k)
                    vec_pi_minus.SetPtEtaPhiM(track_pt[index_piminus], track_eta[index_piminus],track_phi[index_piminus], mass_pi)
                    vec_pi_plus1.SetPtEtaPhiM(track_pt[index_piplus1], track_eta[index_piplus1],track_phi[index_piplus1], mass_pi)
                    vec_pi_plus2.SetPtEtaPhiM(track_pt[index_piplus2], track_eta[index_piplus2],track_phi[index_piplus2], mass_pi)

                    vec_D0_right = vec_K_minus + vec_pi_plus1 + vec_pi_plus2 + vec_pi_minus
                    m_D0_right = vec_D0_right.M()
#Correct mass histogram
                    h_D0_decaymright.Fill(m_D0_right)

                    print('m right', m_D0_right)

                    vec_K_minus_wrong.SetPtEtaPhiM(track_pt[index_k], track_eta[index_k],track_phi[index_k], mass_pi)
                    vec_pi_minus_wrong.SetPtEtaPhiM(track_pt[index_piminus], track_eta[index_piminus],track_phi[index_piminus], mass_k)
                    vec_pi_plus1_wrong.SetPtEtaPhiM(track_pt[index_piplus1], track_eta[index_piplus1],track_phi[index_piplus1], mass_pi)
                    vec_pi_plus2_wrong.SetPtEtaPhiM(track_pt[index_piplus2], track_eta[index_piplus2],track_phi[index_piplus2], mass_pi)

                    vec_D0_wrong = vec_K_minus_wrong + vec_pi_plus1_wrong + vec_pi_plus2_wrong + vec_pi_minus_wrong
                    m_D0_wrong = vec_D0_wrong.M()

                    print('m wrong', m_D0_wrong)

#Incorrect mass histogram
                    h_D0_decaymwrong.Fill(m_D0_wrong)

    stop_iteration += 1

    if stop_iteration > 10000: break
=======
        print("Event number: %i"%stop_iteration)
        mass = [m for m in event.DMesons_NOSYS_m]
        pt = [p for p in event.DMesons_NOSYS_pt]
        pdgID = [i for i in event.DMesons_NOSYS_pdgId]

        mKpi1 = [m for m in event.DMesons_NOSYS_mKpi1]
        mKpi2 = [m for m in event.DMesons_NOSYS_mKpi2]

        print("D-meson mass: ", mass)
        print("D-meson pt: ", pt)
        print("D-meson pdgID", pdgID)
        print("mKpi1", mKpi1)
        print("mKpi2", mKpi2)
        if pdgID[0] == 421 or pdgID[0] == -421: 
            h_m.Fill(mass[0])

        stop_iteration += 1

        if stop_iteration > 10 : break

>>>>>>> f5b0fdd22a948105a207a3f308c0d9ecb799d346


outfile.cd()
h_m.Write()
<<<<<<< HEAD
h_p.Write()
h_D0_decaymright.Write()
h_D0_decaymwrong.Write()
outfile.Close()

=======
outfile.Close()
>>>>>>> f5b0fdd22a948105a207a3f308c0d9ecb799d346
