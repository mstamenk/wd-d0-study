import ROOT, os
from array import array

ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasLabels.C")
ROOT.SetAtlasStyle()

path_to_files = '/global/cfs/cdirs/atlas/wcharm/v10_spg_D0/merged/'
filename = 'SPG.ForcedDecay.999911.root'

tree = ROOT.TChain('CharmAnalysis')
tree.AddFile(path_to_files + filename)

outfile = ROOT.TFile('output_run_tree.root','recreate')

h_m = ROOT.TH1F('m','m',50, 1700,2200)
h_p = ROOT.TH1F('pt','pt',50, 5000,100000)
h_D0_decaymright = ROOT.TH1F('m_D0_right', 'm_D0_right', 50, 1700, 2200)
h_D0_decaymwrong = ROOT.TH1F('m_D0_wrong', 'm_D0_wrong', 50, 1700, 2200)
mass_k = 493.677 #MeV
mass_pi = 139.570 #MeV

count_true_D0 = 0
count_reco_D0 = 0

stop_iteration = 0
for event in tree:
    if event.DMesons_NOSYS_m.size() > 0 :
        pdgID = [i for i in event.DMesons_NOSYS_pdgId]
        mass = [m for m in event.DMesons_NOSYS_m]
        true_pdgID = [i for i in event.TruthParticles_Selected_pdgId]
        if true_pdgID.count(-421):
            barCode = [d for d in event.DMesons_NOSYS_daughterInfo__truthBarcode]           
            true_barCode = [d for d in event.TruthParticles_Selected_daughterInfoT__barcode.at(0)]
            daughter_pdgID = [d for d in event.DMesons_NOSYS_daughterInfo__pdgId]           
            true_daughter_pdgID = [d for d in event.TruthParticles_Selected_daughterInfoT__pdgId.at(0)]
            if true_daughter_pdgID.count(321) == 1 and true_daughter_pdgID.count(-211) == 2 and true_daughter_pdgID.count(211) == 1:
                count_true_D0 += 1
                print('true_daughter_pdgID',[el for el in true_daughter_pdgID])
                print('true_barCode', [el for el in true_barCode])
                print('pdgID', [el for el in pdgID])
                print('mass', [el for el in mass])
                print("")

                for i in range(len(barCode)):
                    print('Reco #', i)
                    print('barCode',[el for el in barCode[i]])
                    print('daughter_pdgID', [el for el in daughter_pdgID[i]])
                    print("")

                    if pdgID[i] == -421:
                        
                        # Fill hisograms
                        # if statment (correct reco)
                            #fill h_correct
                        # else statement (wrong reco)
                            # fill h_wrong
                        # where to fill count_reco_D0 += 1



        if pdgID.count(-421) == 1:
            indexD0 = pdgID.index(-421)
            mass = [m for m in event.DMesons_NOSYS_m]
            pt = [p for p in event.DMesons_NOSYS_pt]
            daughter_pdgID = [d for d in event.DMesons_NOSYS_daughterInfo__pdgId.at(indexD0)]           
            #print(mass)
            #print(pt)
            #print(daughter_pdgID)
            
            if len(daughter_pdgID) == 4: 
                if daughter_pdgID.count(321) == 1 and daughter_pdgID.count(-211) == 2 and daughter_pdgID.count(211) == 1:
                    #print(pdgID[indexD0], mass[indexD0])
                    h_m.Fill(mass[indexD0])
                    
                    #print('D0 mass', mass[indexD0])

                    track_pt = [i for i in event.DMesons_NOSYS_daughterInfo__pt.at(indexD0)]
                    track_eta = [i for i in event.DMesons_NOSYS_daughterInfo__eta.at(indexD0)]
                    track_phi = [i for i  in event.DMesons_NOSYS_daughterInfo__phi.at(indexD0)]

                    vec_D0_right = ROOT.TLorentzVector()
                    vec_K_minus = ROOT.TLorentzVector()
                    vec_pi_plus1 = ROOT.TLorentzVector()
                    vec_pi_plus2 = ROOT.TLorentzVector()
                    vec_pi_minus = ROOT.TLorentzVector()
                    vec_D0_wrong = ROOT.TLorentzVector()
                    vec_K_minus_wrong = ROOT.TLorentzVector()
                    vec_pi_plus1_wrong = ROOT.TLorentzVector()
                    vec_pi_plus2_wrong = ROOT.TLorentzVector()
                    vec_pi_minus_wrong = ROOT.TLorentzVector()

                    #print('pdgid', pdgID)
                    #print('daughter pdgid', daughter_pdgID)
                    #print('track pt:', track_pt)
                    #print('track eta:', track_eta)
                    #print('track phi:', track_phi)
                    
                    index_k= daughter_pdgID.index(321)
                    index_piminus = daughter_pdgID.index(211)
                    index_piplus1 = daughter_pdgID.index(-211)
                    indices = [0, 1, 2, 3]
                    indices.remove(index_k)
                    indices.remove(index_piminus)
                    indices.remove(index_piplus1)
                    index_piplus2 = indices[0]
                    #print(index_k ,index_piminus, index_piplus1, index_piplus2)

                    vec_K_minus.SetPtEtaPhiM(track_pt[index_k], track_eta[index_k],track_phi[index_k], mass_k)
                    vec_pi_minus.SetPtEtaPhiM(track_pt[index_piminus], track_eta[index_piminus],track_phi[index_piminus], mass_pi)
                    vec_pi_plus1.SetPtEtaPhiM(track_pt[index_piplus1], track_eta[index_piplus1],track_phi[index_piplus1], mass_pi)
                    vec_pi_plus2.SetPtEtaPhiM(track_pt[index_piplus2], track_eta[index_piplus2],track_phi[index_piplus2], mass_pi)

                    vec_D0_right = vec_K_minus + vec_pi_plus1 + vec_pi_plus2 + vec_pi_minus
                    m_D0_right = vec_D0_right.M()
#Correct mass histogram
                    h_D0_decaymright.Fill(m_D0_right)

                    #print('m right', m_D0_right)

                    vec_K_minus_wrong.SetPtEtaPhiM(track_pt[index_k], track_eta[index_k],track_phi[index_k], mass_pi)
                    vec_pi_minus_wrong.SetPtEtaPhiM(track_pt[index_piminus], track_eta[index_piminus],track_phi[index_piminus], mass_k)
                    vec_pi_plus1_wrong.SetPtEtaPhiM(track_pt[index_piplus1], track_eta[index_piplus1],track_phi[index_piplus1], mass_pi)
                    vec_pi_plus2_wrong.SetPtEtaPhiM(track_pt[index_piplus2], track_eta[index_piplus2],track_phi[index_piplus2], mass_pi)

                    vec_D0_wrong = vec_K_minus_wrong + vec_pi_plus1_wrong + vec_pi_plus2_wrong + vec_pi_minus_wrong
                    m_D0_wrong = vec_D0_wrong.M()

                    #print('m wrong', m_D0_wrong)

#Incorrect mass histogram
                    h_D0_decaymwrong.Fill(m_D0_wrong)

    stop_iteration += 1

    if stop_iteration > 1000: break

outfile.cd()
h_m.Write()
h_p.Write()
h_D0_decaymright.Write()
h_D0_decaymwrong.Write()
outfile.Close()

