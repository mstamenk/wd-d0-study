# Script to plot one variable from the outputs of the W+D analysis
# author: Marko Stamenkovic

# import section
import os, ROOT

# Setup ATLAS style for plotting
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasStyle.C")
ROOT.gROOT.ProcessLine(".L atlasstyle/AtlasLabels.C")
ROOT.SetAtlasStyle()

# Initialise the file from which to read the histograms

pwd = os.getenv('MYROOT') + '/' # get the path to the repository where the 'source setup.sh' was done
path_to_samples = pwd + 'samples/' # path to samples folder

filename = 'data.root' # example here using the data

f = ROOT.TFile(path_to_samples + filename, 'read') # read the file with the histograms

# Getting one histogram to be plotted

# Get the histogram of the opposite sign, negative electron, 0 tag category
hist_name = 'el_minus_SR_0tag_Dplus_OS__Dmeson_m' # D-meson mass
h_el_minus_1tag_OS = f.Get(hist_name) 

# Get the histogram of the same sign, negative electron, 0 tag category
hist_name = 'el_minus_SR_0tag_Dplus_SS__Dmeson_m' # D-meson mass
h_el_minus_1tag_SS = f.Get(hist_name) 

# Create histogram of opposite sign - same sign
h_el_minus_1tag_OS_SS = h_el_minus_1tag_OS.Clone(h_el_minus_1tag_OS.GetName() + '_OS_SS') # Clone the OS hist with different name
h_el_minus_1tag_OS_SS.Add(h_el_minus_1tag_SS,-1) # Substract the SS histogram

# Prepare the plots 
h_el_minus_1tag_OS.SetLineColor(ROOT.kBlue + 2) # Change line color for plot
h_el_minus_1tag_SS.SetLineColor(ROOT.kRed + 2)
h_el_minus_1tag_OS.SetMarkerSize(0)
h_el_minus_1tag_SS.SetMarkerSize(0)

# OS - SS
h_el_minus_1tag_OS_SS.SetLineColor(ROOT.kBlack)
h_el_minus_1tag_OS_SS.SetMarkerSize(0)

# Make plot look nice together on the canvas
factor = 1.8
maxi = max(h_el_minus_1tag_OS.GetMaximum(), (h_el_minus_1tag_SS.GetMaximum()))
h_el_minus_1tag_OS.SetMaximum(factor * maxi)
h_el_minus_1tag_SS.SetMaximum(factor * maxi)

h_el_minus_1tag_OS_SS.SetMaximum(factor * h_el_minus_1tag_OS_SS.GetMaximum())

# Create legend for first plot
legend1 = ROOT.TLegend(0.6,0.5,0.9,0.7) # Place legend in the top right corner, starting just after the middle
legend1.SetBorderSize(0) # make legend look nice without border

legend1.AddEntry(h_el_minus_1tag_OS, 'Opposite sign','f')
legend1.AddEntry(h_el_minus_1tag_SS, 'Same sign', 'f')

# Plot histograms: first canvas, draw OS and SS on the same plot
c1 = ROOT.TCanvas()
h_el_minus_1tag_OS.Draw('hist e') # Draw histogram, with error bar on the bins
h_el_minus_1tag_SS.Draw('hist e same') # Draw second hist on the same canvas
legend1.Draw()

# Add text in the plot
ROOT.ATLASLabel(0.2,0.88,"Internal") # ATLAS label for internal plots
ROOT.myText(0.2,0.82,1,"#sqrt{s}= 13 TeV, %s fb^{-1}"%('139'))
ROOT.myText(0.2,0.76,1,'D-meson, D^{+}#rightarrow K#pi#pi, e^{-}-channel, 0 b-tags')

# Save plot
c1.Print('C_example_OS_SS_comparison.pdf')

# Plot histograms: first canvas, draw OS and SS on the same plot
c2 = ROOT.TCanvas()
h_el_minus_1tag_OS_SS.Draw('hist e') # Draw histogram, with error bar on the bins

# Add text in the plot
ROOT.ATLASLabel(0.2,0.88,"Internal") # ATLAS label for internal plots
ROOT.myText(0.2,0.82,1,"#sqrt{s}= 13 TeV, %s fb^{-1}"%('139'))
ROOT.myText(0.2,0.76,1,'D-meson, D^{+}#rightarrow K#pi#pi, e^{-}-channel, 0 b-tags')
ROOT.myText(0.2,0.68,1,'OS-SS')

# Save plot
c1.Print('C_example_OS_SS.pdf')




